from glob import glob 
import os 
from threading import Thread

def run_cmd(video, audio, port):
    os.system(f'./test-launch -p {port} "multifilesrc location={video} ! queue ! h264parse ! decodebin ! x264enc speed-preset=ultrafast tune=zerolatency  ! rtph264pay name=pay0 pt=96  multifilesrc location={audio} loop=true ! wavparse ignore-length=true ! audioconvert ! alawenc ! rtppcmapay name=pay1"')

if __name__=="__main__":
    list_videos = glob("videos_h264/*")
    # list_audios = glob("audios_wav/*")
    os.system("gcc gst-launch-rtsp.c -o test-launch $(pkg-config --cflags --libs gstreamer-1.0 gstreamer-rtsp-server-1.0)")
    for idx, video in enumerate(list_videos):
        audio = "audios/example.wav"
        Thread(target=run_cmd, args=(video,audio, idx+100, )).start()