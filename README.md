# Generate Fake RTSP
Generate virtual rtsp stream with gstreamer or vlc

## Prepare env docker and lib

### Install vlc
+ sudo apt-get install vlc <br/>
+ Note: Should not install vlc with snap

### Install docker unbuntu
https://docs.docker.com/engine/install/ubuntu

### Pull docker gstreamer 
+ docker pull restreamio/gstreamer:latest-dev

### Prepare video, audio

+ Videos .h264, .h265 and audios format .wav, .aac, .ac3

+ Camera Hikvision, KBvision, ... videos often have encoder h264 or h265 <br/>
and audio often have encoder  G722.1; G711.alaw; G711.ulaw ; MP2L2 ; G726 ; PCM


## Run 
docker run -it --rm --network host --privileged -v $PWD:/export_rtsp restreamio/gstreamer:latest-dev <br/>
cd /export_rtsp <br/>
+ Test 1 video + 1 audio -> 40 rtsp <br/>
exam: bash gst_export_rtsp.sh <br/>
+ Test mul video + mul audio ->  mul rtsp <br/>
    exam: python3 h264_wav_multi.py
    or python3 h265_wav_multi.py

### Test rtsp
40 rtsp fake generated: f'rtsp://localhost:{i}/gst' for i in {1..40} <br/>
+ using vlc to check rtsp: vlc 'rtsp://localhost:1/gst'
+ using ffmpeg : ffplay 'rtsp://localhost:1/gst'
### Kill all rtsp
+ bash kill_pgst.sh

## Solution diffirence using vlc

Note: Using vlc export rtsp fake have video but audio only format MPEG-audio <br/>
run comand: cvlc videos/youtube.mp4 :sout=#gather:rtp{sdp=rtsp://0.0.0.0:3000/vlc} :network-caching=1500 :sout-all :sout-keep --sout --loop --repeat <br/>

+ test with ffmpeg: ffplay 'rtsp://localhost:3000/vlc' <br/>
+ test with gst: gst-play-1.0 'rtsp://localost:3000/vlc' <br/>
