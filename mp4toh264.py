#docker run -v $(pwd):$(pwd) -w $(pwd) jrottenberg/ffmpeg -i bancong_1.mp4 -an -vcodec libx264 -crf 23 bancong.h264
import os 
from glob import glob 
from threading import Thread


def convert_h264(input, path_save):
    filename = input.split("/")[-1][:-4]
    output = os.path.join(path_save, f"{filename}.h264")
    os.system(f"docker run --rm -v $(pwd):$(pwd) -w $(pwd) jrottenberg/ffmpeg -i {input} -an -vcodec libx264 -crf 23 {output}")
    print("convert {} to h264 done !".format(filename))

if __name__== "__main__":
    lis_videos = glob("videos_mp4/*")
    path_save = "videos_h264"
    for idx, input in enumerate(lis_videos):
        convert_h264(input, path_save)
