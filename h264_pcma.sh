gcc gst-launch-rtsp.c -o test-launch $(pkg-config --cflags --libs gstreamer-1.0 gstreamer-rtsp-server-1.0)
for i in {100..300}
do
    ( ./test-launch -p $i "multifilesrc location=videos/youtube.h264 ! queue ! h264parse ! decodebin ! \
    x264enc speed-preset=ultrafast tune=zerolatency  ! rtph264pay name=pay0 pt=96  multifilesrc location=audios/example.wav \
    loop=true ! wavparse ignore-length=true ! audioconvert ! alawenc ! rtppcmapay name=pay1" & )
done