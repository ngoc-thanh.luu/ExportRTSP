#docker run -v $(pwd):$(pwd) -w $(pwd) jrottenberg/ffmpeg -i bancong_1.mp4 -an -vcodec libx264 -crf 23 bancong.h264
import os 
from glob import glob 
from threading import Thread


def convert_wav(input, path_save):
    filename = input.split("/")[-1][:-4]
    output = os.path.join(path_save, f"{filename}.wav")
    os.system(f"docker run -v $(pwd):$(pwd) -w $(pwd) jrottenberg/ffmpeg -i {input} -acodec pcm_s16le -ac 1 -ar 16000 {output}")
    print("convert {} to h264 done !".format(filename))

if __name__== "__main__":
    lis_videos = glob("audios_mp3/*")
    path_save = "audios_wav"
    for idx, input in enumerate(lis_videos):
        convert_wav(input, path_save)
